/******************************************************************************
 * mal - memory allocation library (test)
 * Tue Feb 12 21:59:27 GMT 2013
 *
 * Copyright (C) 2013-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of mal.
 *
 * mal is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * mal is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with mal; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2013-02-18 Check internal counts.
 * 2013-03-05 Demonstrate clean-up after memory leaks.
 * 2013-03-16 Fix malloc balance when pointers have not been freed.
 * 2013-03-19 Test a double-free.
 * 2013-03-20 Add functions to allocate and free with text labels.
 * 2013-06-09 Add functions to get current size in use and max size used.
 * 2013-07-20 Test whether a pointer is valid (known and in use).
 * 2014-05-28 Improvements.
 * 2020-01-18 Add posix_memalign functions.
 * 2020-01-19 Add multimalloc.
 * 2020-01-20 Add multifree.
 * 2022-12-29 Add register_with_labels.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "mal.h"
#include "mal_private.h"
#include "cutl.h"

void test_mal_open_close(void);
void test_mal_malloc_free(void);
void test_mal_leak(void);
void test_mal_double_free(void);
void test_mal_malloc_free_with_labels(void);
void test_mal_is_valid(void);
void test_mal_posix_memalign_free(void);
void test_mal_multimalloc_free(void);
void test_mal_multimalloc_free_many(void);
void test_mal_register_free_with_labels(void);

cutl_test_case_t tests[] =
{
    { "mal_open_close",                test_mal_open_close                },
    { "mal_malloc_free",               test_mal_malloc_free               },
    { "mal_leak",                      test_mal_leak                      },
    { "mal_double_free",               test_mal_double_free               },
    { "mal_malloc_free_with_labels",   test_mal_malloc_free_with_labels   },
    { "mal_is_valid",                  test_mal_is_valid                  },
    { "mal_posix_memalign_free",       test_mal_posix_memalign_free       },
    { "mal_multimalloc_free",          test_mal_multimalloc_free          },
    { "mal_multimalloc_free_many",     test_mal_multimalloc_free_many     },
    { "mal_register_free_with_labels", test_mal_register_free_with_labels },
};

void mal_test_case_setup();
void mal_test_case_teardown();

void mal_test_case_setup()
{
    cutl_stdlib_test_stubs_reset();
}

void mal_test_case_teardown()
{
}

int main(int argc, char *argv[])
{
    printf("mal test.\n");

	cutl_set_test_lib("mal", tests, NO_OF_ELEMENTS(tests));
	cutl_set_cutl_test_setup(mal_test_case_setup);
	cutl_set_cutl_test_teardown(mal_test_case_teardown);
	cutl_get_options(argc, argv);
	cutl_run_tests();

    return 0;
}

void internal_check_ptrs_clear(int32_t nptrs)
{
    int32_t i;

    for (i = 0; i < nptrs; i++)
    {
        TEST_INT32_EQUAL("mal_index_is_valid()", 1, mal_index_is_valid(i));
        TEST_PTR_EQUAL("ptr", NULL, mal_get_ptr_info_ptr(i));
        TEST_SIZE_T_EQUAL("size", 0, mal_get_ptr_info_size(i));
        TEST_INT32_EQUAL("allocated", 0, mal_get_ptr_info_allocated(i));
        TEST_INT32_EQUAL("seen_count", 0, mal_get_ptr_info_seen_count(i));
        TEST_PTR_EQUAL("alloc_str", NULL, mal_get_ptr_info_alloc_str(i));
        TEST_INT32_EQUAL("alloc_str_len", 0, mal_get_ptr_info_alloc_str_len(i));
        TEST_PTR_EQUAL("free_str", NULL, mal_get_ptr_info_free_str(i));
        TEST_INT32_EQUAL("free_str_len", 0, mal_get_ptr_info_free_str_len(i));
    }
}

void test_mal_open_close(void)
{
    int32_t nptrs;

    nptrs = mal_open(0);
    TEST_INT32_EQUAL("mal_open()", nptrs, MAL_DEFAULT_PTRS);
    TEST_INT32_EQUAL("exit_count()", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", 0, mal_get_max_total_size_used());

    internal_check_ptrs_clear(nptrs);

    nptrs = mal_open(0);
    TEST_INT32_EQUAL("mal_open()", nptrs, 0);
    TEST_INT32_EQUAL("exit_count", 1, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("mal_close()", mal_close(), 0);
    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", 0, mal_get_max_total_size_used());
}

void test_mal_malloc_free(void)
{
    int32_t nptrs;
    void *test_ptr;
    void *confirm_ptr;
    int32_t test_ptr_index;
    int32_t confirm_ptr_index;
    int32_t invalid_index;
    size_t confirm_size;

    mal_set_counting_exit_count(0);

    nptrs = mal_open(10);
    TEST_INT32_EQUAL("mal_open()", nptrs, 10);
    TEST_INT32_EQUAL("exit_count()", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", 0, mal_get_max_total_size_used());

    internal_check_ptrs_clear(nptrs);

    test_ptr = mal_malloc(sizeof(int32_t));
    TEST_PTR_NOT_NULL("mal_malloc()", test_ptr);
    test_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_NOT_EQUAL("test_ptr_index", -1, test_ptr_index);

    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 1, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));

    confirm_ptr = mal_get_ptr_info_ptr(test_ptr_index);
    TEST_PTR_EQUAL("confirm_ptr", test_ptr, confirm_ptr);

    confirm_size = mal_get_ptr_info_size(test_ptr_index);
    TEST_SIZE_T_EQUAL("confirm_size", sizeof(int32_t), confirm_size);

    TEST_SIZE_T_EQUAL("total_size_in_use", sizeof(int32_t), mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", sizeof(int32_t), mal_get_max_total_size_used());

    invalid_index = mal_find_ptr((void *)0xDEADBEEF);
    TEST_INT32_EQUAL("invalid_index", -1, invalid_index);

    mal_free(test_ptr);
    confirm_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_EQUAL("confirm_ptr_index", test_ptr_index, confirm_ptr_index);
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 0, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));
    confirm_size = mal_get_ptr_info_size(test_ptr_index);
    TEST_SIZE_T_EQUAL("confirm_size", sizeof(int32_t), confirm_size);
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", sizeof(int32_t), mal_get_max_total_size_used());

    TEST_INT32_EQUAL("exit_count", 0, mal_get_counting_exit_count());

    mal_free((void *)0xDEADBEEF);
    TEST_INT32_EQUAL("exit_count", 1, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("mal_close()", mal_close(), 0);
    TEST_INT32_EQUAL("malloc_count", 1, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 1, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
}

void test_mal_leak(void)
{
    int32_t nptrs;
    void *test_ptr;
    void *confirm_ptr;
    int32_t test_ptr_index;

    mal_set_counting_exit_count(0);

    nptrs = mal_open(10);
    TEST_INT32_EQUAL("mal_open()", nptrs, 10);
    TEST_INT32_EQUAL("exit_count()", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());

    internal_check_ptrs_clear(nptrs);

    test_ptr = mal_malloc(sizeof(int32_t));
    TEST_PTR_NOT_NULL("mal_malloc()", test_ptr);
    test_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_NOT_EQUAL("test_ptr_index", -1, test_ptr_index);
    confirm_ptr = mal_get_ptr_info_ptr(test_ptr_index);
    TEST_PTR_EQUAL("confirm_ptr", test_ptr, confirm_ptr);

    TEST_INT32_EQUAL("mal_close()", 1, mal_close());
    /* FIXME: This is not measuring the right thing! */
    TEST_INT32_EQUAL("malloc_count", 1, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 1, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
}

void test_mal_double_free(void)
{
    int32_t nptrs;
    void *test_ptr;
    void *confirm_ptr;
    int32_t test_ptr_index;
    int32_t confirm_ptr_index;

    mal_set_counting_exit_count(0);

    nptrs = mal_open(10);
    TEST_INT32_EQUAL("mal_open()", nptrs, 10);
    TEST_INT32_EQUAL("exit_count()", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());

    internal_check_ptrs_clear(nptrs);

    test_ptr = mal_malloc(sizeof(int32_t));
    TEST_PTR_NOT_NULL("mal_malloc()", test_ptr);
    test_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_NOT_EQUAL("test_ptr_index", -1, test_ptr_index);

    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 1, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));

    confirm_ptr = mal_get_ptr_info_ptr(test_ptr_index);
    TEST_PTR_EQUAL("confirm_ptr", test_ptr, confirm_ptr);

    mal_free(test_ptr);
    TEST_INT32_EQUAL("exit_count", 0, mal_get_counting_exit_count());
    confirm_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_EQUAL("confirm_ptr_index", test_ptr_index, confirm_ptr_index);
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 0, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));

    mal_free(test_ptr);
    TEST_INT32_EQUAL("exit_count", 1, mal_get_counting_exit_count());
    confirm_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_EQUAL("confirm_ptr_index", test_ptr_index, confirm_ptr_index);
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 0, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));

    TEST_INT32_EQUAL("mal_close()", mal_close(), 0);
    TEST_INT32_EQUAL("malloc_count", 1, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 1, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
}

void test_mal_malloc_free_with_labels(void)
{
    int32_t nptrs;
    void *test_ptr;
    void *confirm_ptr;
    int32_t test_ptr_index;
    int32_t confirm_ptr_index;
    int32_t invalid_index;

    mal_set_counting_exit_count(0);

    nptrs = mal_open(10);
    TEST_INT32_EQUAL("mal_open()", nptrs, 10);
    TEST_INT32_EQUAL("exit_count()", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());

    internal_check_ptrs_clear(nptrs);

    test_ptr = mal_malloc_with_labels(sizeof(int32_t), "file1", "function1", 1);
    TEST_PTR_NOT_NULL("mal_malloc_with_labels()", test_ptr);
    test_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_NOT_EQUAL("test_ptr_index", -1, test_ptr_index);

    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 1, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));
    TEST_PTR_NOT_NULL("mal_get_ptr_info_alloc_str()", mal_get_ptr_info_alloc_str(test_ptr_index));
    printf("%s\n", mal_get_ptr_info_alloc_str(test_ptr_index));

    confirm_ptr = mal_get_ptr_info_ptr(test_ptr_index);
    TEST_PTR_EQUAL("confirm_ptr", test_ptr, confirm_ptr);

    invalid_index = mal_find_ptr((void *)0xDEADBEEF);
    TEST_INT32_EQUAL("invalid_index", -1, invalid_index);

    mal_free_with_labels(test_ptr, "file2", "function2", 2);
    confirm_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_EQUAL("confirm_ptr_index", test_ptr_index, confirm_ptr_index);
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 0, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));
    TEST_PTR_NOT_NULL("mal_get_ptr_info_free_str()", mal_get_ptr_info_free_str(test_ptr_index));
    printf("%s\n", mal_get_ptr_info_free_str(test_ptr_index));

    TEST_INT32_EQUAL("exit_count", 0, mal_get_counting_exit_count());
    mal_free_with_labels((void *)0xDEADBEEF, "file3", "function3", 3);
    TEST_INT32_EQUAL("exit_count", 1, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("mal_close()", mal_close(), 0);
    TEST_INT32_EQUAL("malloc_count", 1, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 1, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
}

#ifndef NO_OF_ELEMENTS
#define NO_OF_ELEMENTS(array) ((int)(sizeof(array)/sizeof(array[0])))
#endif
#define NTEST_PTRS      10
void test_mal_is_valid(void)
{
    int32_t nptrs;
    void *test_ptr[NTEST_PTRS];
    int32_t i;

    mal_set_counting_exit_count(0);

    nptrs = mal_open(NTEST_PTRS);
    TEST_INT32_EQUAL("mal_open()", nptrs, NTEST_PTRS);
    TEST_INT32_EQUAL("exit_count()", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", 0, mal_get_max_total_size_used());

    internal_check_ptrs_clear(nptrs);

    for(i = 0; i < NO_OF_ELEMENTS(test_ptr); i++)
    {
        test_ptr[i] = mal_malloc(sizeof(int32_t));
        TEST_PTR_NOT_NULL("mal_malloc()", test_ptr[i]);
    }

    for(i = 0; i < NO_OF_ELEMENTS(test_ptr); i++)
    {
        TEST_INT32_EQUAL("mal_is_valid()", 1, mal_is_valid(test_ptr[i]));
    }

    TEST_INT32_EQUAL("mal_is_valid()", 0, mal_is_valid((void *)0xDEADBEEF));
    TEST_INT32_EQUAL("mal_is_valid()", 0, mal_is_valid(NULL));

    for(i = 0; i < NO_OF_ELEMENTS(test_ptr); i++)
    {
        mal_free(test_ptr[i]);
    }

    for(i = 0; i < NO_OF_ELEMENTS(test_ptr); i++)
    {
        TEST_INT32_EQUAL("mal_is_valid()", 0, mal_is_valid(test_ptr[i]));
    }

    TEST_INT32_EQUAL("mal_close()", mal_close(), 0);
}

void test_mal_posix_memalign_free(void)
{
    int32_t nptrs;
    void *test_ptr;
	int32_t retcode;
    void *confirm_ptr;
    int32_t test_ptr_index;
    int32_t confirm_ptr_index;
    int32_t invalid_index;
    size_t confirm_size;

    mal_set_counting_exit_count(0);

    nptrs = mal_open(10);
    TEST_INT32_EQUAL("mal_open()", nptrs, 10);
    TEST_INT32_EQUAL("exit_count()", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", 0, mal_get_max_total_size_used());

    internal_check_ptrs_clear(nptrs);

	retcode = mal_posix_memalign(&test_ptr, sizeof(void *), sizeof(int32_t));
	TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_PTR_NOT_NULL("mal_posix_memalign()", test_ptr);
    test_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_NOT_EQUAL("test_ptr_index", -1, test_ptr_index);

    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 1, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));

    confirm_ptr = mal_get_ptr_info_ptr(test_ptr_index);
    TEST_PTR_EQUAL("confirm_ptr", test_ptr, confirm_ptr);

    confirm_size = mal_get_ptr_info_size(test_ptr_index);
    TEST_SIZE_T_EQUAL("confirm_size", sizeof(int32_t), confirm_size);

    TEST_SIZE_T_EQUAL("total_size_in_use", sizeof(int32_t), mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", sizeof(int32_t), mal_get_max_total_size_used());

    invalid_index = mal_find_ptr((void *)0xDEADBEEF);
    TEST_INT32_EQUAL("invalid_index", -1, invalid_index);

    mal_free(test_ptr);
    confirm_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_EQUAL("confirm_ptr_index", test_ptr_index, confirm_ptr_index);
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 0, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));
    confirm_size = mal_get_ptr_info_size(test_ptr_index);
    TEST_SIZE_T_EQUAL("confirm_size", sizeof(int32_t), confirm_size);
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", sizeof(int32_t), mal_get_max_total_size_used());

    TEST_INT32_EQUAL("exit_count", 0, mal_get_counting_exit_count());

    mal_free((void *)0xDEADBEEF);
    TEST_INT32_EQUAL("exit_count", 1, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("mal_close()", mal_close(), 0);
    TEST_INT32_EQUAL("malloc_count", 1, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 1, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
}

void test_mal_multimalloc_free(void)
{
    int32_t nptrs;
    void **test_ptr = NULL;
    int32_t retcode;
    void *confirm_ptr;
    int32_t test_ptr_index;
    int32_t data_ptr_index;
    int32_t confirm_ptr_index;
    int32_t invalid_index;
    size_t confirm_size;

    mal_set_counting_exit_count(0);

    nptrs = mal_open(10);
    TEST_INT32_EQUAL("mal_open()", nptrs, 10);
    TEST_INT32_EQUAL("exit_count()", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", 0, mal_get_max_total_size_used());

    internal_check_ptrs_clear(nptrs);

    retcode = mal_multimalloc(&test_ptr, sizeof(int32_t), 0);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_PTR_NULL("mal_malloc()", test_ptr);

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", 0, mal_get_max_total_size_used());

    retcode = mal_multimalloc(&test_ptr, 0, 1);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_PTR_NULL("mal_malloc()", test_ptr);

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", 0, mal_get_max_total_size_used());

    retcode = mal_multimalloc(&test_ptr, sizeof(int32_t), 1);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_PTR_NOT_NULL("mal_malloc()", test_ptr);
    test_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_NOT_EQUAL("test_ptr_index", -1, test_ptr_index);
    data_ptr_index = mal_find_ptr(test_ptr[0]);
    TEST_INT32_NOT_EQUAL("data_ptr_index", -1, data_ptr_index);

    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 1, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 1, mal_get_ptr_info_allocated(data_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(data_ptr_index));

    confirm_ptr = mal_get_ptr_info_ptr(test_ptr_index);
    TEST_PTR_EQUAL("confirm_ptr", test_ptr, confirm_ptr);

    confirm_size = mal_get_ptr_info_size(test_ptr_index);
    TEST_SIZE_T_EQUAL("confirm_size", sizeof(void *), confirm_size);

    TEST_SIZE_T_EQUAL("total_size_in_use", sizeof(int32_t) + sizeof(void *), mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", sizeof(int32_t) + sizeof(void *), mal_get_max_total_size_used());

    invalid_index = mal_find_ptr((void *)0xDEADBEEF);
    TEST_INT32_EQUAL("invalid_index", -1, invalid_index);

    mal_multifree(test_ptr);
    confirm_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_EQUAL("confirm_ptr_index", test_ptr_index, confirm_ptr_index);
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 0, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 0, mal_get_ptr_info_allocated(data_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(data_ptr_index));

    confirm_size = mal_get_ptr_info_size(test_ptr_index);
    TEST_SIZE_T_EQUAL("confirm_size", sizeof(void *), confirm_size);
    confirm_size = mal_get_ptr_info_size(data_ptr_index);
    TEST_SIZE_T_EQUAL("confirm_size", sizeof(int32_t), confirm_size);
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", sizeof(int32_t) + sizeof(void *), mal_get_max_total_size_used());

    TEST_INT32_EQUAL("exit_count", 0, mal_get_counting_exit_count());

    mal_free((void *)0xDEADBEEF);
    TEST_INT32_EQUAL("exit_count", 1, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("mal_close()", mal_close(), 0);
    TEST_INT32_EQUAL("malloc_count", 2, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 2, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
}

#define NO_OF_OBJECTS    4
void test_mal_multimalloc_free_many(void)
{
    int32_t nptrs;
    void **test_ptr = NULL;
    int32_t retcode;
    void *confirm_ptr;
    int32_t test_ptr_index;
    int32_t data_ptr_index;
    int32_t confirm_ptr_index;
    size_t confirm_size;
    int32_t i;

    mal_set_counting_exit_count(0);

    nptrs = mal_open(10);
    TEST_INT32_EQUAL("mal_open()", nptrs, 10);
    TEST_INT32_EQUAL("exit_count()", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", 0, mal_get_max_total_size_used());

    internal_check_ptrs_clear(nptrs);

    retcode = mal_multimalloc(&test_ptr, sizeof(int32_t), NO_OF_OBJECTS);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_PTR_NOT_NULL("mal_malloc()", test_ptr);
    test_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_NOT_EQUAL("test_ptr_index", -1, test_ptr_index);
    data_ptr_index = mal_find_ptr(test_ptr[0]);
    TEST_INT32_NOT_EQUAL("data_ptr_index", -1, data_ptr_index);

    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 1, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 1, mal_get_ptr_info_allocated(data_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(data_ptr_index));

    confirm_ptr = mal_get_ptr_info_ptr(test_ptr_index);
    TEST_PTR_EQUAL("confirm_ptr", test_ptr, confirm_ptr);

    confirm_size = mal_get_ptr_info_size(test_ptr_index);
    TEST_SIZE_T_EQUAL("confirm_size", sizeof(void *) * NO_OF_OBJECTS, confirm_size);

    TEST_SIZE_T_EQUAL("total_size_in_use", (sizeof(int32_t) * NO_OF_OBJECTS) + (sizeof(void *) * NO_OF_OBJECTS),
                                           mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", (sizeof(int32_t) * NO_OF_OBJECTS) + (sizeof(void *) * NO_OF_OBJECTS),
                                            mal_get_max_total_size_used());
     
    for (i = 1; i < NO_OF_OBJECTS; i++)
    {
        TEST_PTR_EQUAL("address", test_ptr[i - 1] + sizeof(uint32_t), test_ptr[i]);
    }


    mal_multifree(test_ptr);
    confirm_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_EQUAL("confirm_ptr_index", test_ptr_index, confirm_ptr_index);
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 0, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 0, mal_get_ptr_info_allocated(data_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(data_ptr_index));

    confirm_size = mal_get_ptr_info_size(test_ptr_index);
    TEST_SIZE_T_EQUAL("confirm_size", sizeof(void *) * NO_OF_OBJECTS, confirm_size);
    confirm_size = mal_get_ptr_info_size(data_ptr_index);
    TEST_SIZE_T_EQUAL("confirm_size", sizeof(int32_t) * NO_OF_OBJECTS, confirm_size);
    TEST_SIZE_T_EQUAL("total_size_in_use", 0, mal_get_total_size_in_use());
    TEST_SIZE_T_EQUAL("max_total_size_used", (sizeof(int32_t) * NO_OF_OBJECTS) + (sizeof(void *) * NO_OF_OBJECTS),
                                             mal_get_max_total_size_used());

    TEST_INT32_EQUAL("exit_count", 0, mal_get_counting_exit_count());
    TEST_INT32_EQUAL("mal_close()", mal_close(), 0);
    TEST_INT32_EQUAL("malloc_count", 2, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 2, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
}

void test_mal_register_free_with_labels(void)
{
    int32_t nptrs;
    void *test_ptr;
    void *confirm_ptr;
    int32_t test_ptr_index;
    int32_t confirm_ptr_index;
    char *print_str = NULL;

    mal_set_counting_exit_count(0);

    nptrs = mal_open(10);
    TEST_INT32_EQUAL("mal_open()", nptrs, 10);
    TEST_INT32_EQUAL("exit_count()", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 0, mal_get_counting_free_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());

    internal_check_ptrs_clear(nptrs);

    print_str = malloc(strlen("Hello, World!\n") + 1);
    TEST_PTR_NOT_NULL("print_str", print_str);
    strcpy(print_str, "Hello, World!\n");

    test_ptr = mal_register_with_labels(print_str, strlen(print_str) + 1, "file1", "function1", 1);
    test_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_NOT_EQUAL("test_ptr_index", -1, test_ptr_index);

    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 1, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));
    TEST_PTR_NOT_NULL("mal_get_ptr_info_alloc_str()", mal_get_ptr_info_alloc_str(test_ptr_index));
    printf("%s\n", mal_get_ptr_info_alloc_str(test_ptr_index));

    confirm_ptr = mal_get_ptr_info_ptr(test_ptr_index);
    TEST_PTR_EQUAL("confirm_ptr", test_ptr, confirm_ptr);

    confirm_ptr_index = mal_find_ptr(test_ptr);
    TEST_INT32_EQUAL("confirm_ptr_index", test_ptr_index, confirm_ptr_index);
    TEST_INT32_EQUAL("mal_get_ptr_info_allocated()", 1, mal_get_ptr_info_allocated(test_ptr_index));
    TEST_INT32_EQUAL("mal_get_ptr_info_seen_count()", 1, mal_get_ptr_info_seen_count(test_ptr_index));

    MAL_FREE(print_str);

    TEST_PTR_NOT_NULL("mal_get_ptr_info_free_str()", mal_get_ptr_info_free_str(test_ptr_index));
    printf("%s\n", mal_get_ptr_info_free_str(test_ptr_index));

    TEST_INT32_EQUAL("exit_count", 0, mal_get_counting_exit_count());

    TEST_INT32_EQUAL("mal_close()", mal_close(), 0);
    TEST_INT32_EQUAL("malloc_count", 0, mal_get_counting_malloc_count());
    TEST_INT32_EQUAL("free_count", 1, mal_get_counting_free_count());
    TEST_INT32_EQUAL("register_count", 1, mal_get_register_count());
    TEST_INT32_EQUAL("malloc_balance", 0, mal_get_counting_malloc_balance());
}

