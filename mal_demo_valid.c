/******************************************************************************
 * mal_demo.c - memory allocation library demo program.
 * Sun Jul 21 13:04:11 BST 2013
 * Copyright (C) 2013-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of mal.
 *
 * mal is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * mal is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with mal; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2013-07-21 Demonstrate checking of pointer validity.
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include "mal.h"

typedef struct
{
    int32_t i;
    char *c;
}
foo_t;

#define NO_OF_OBJECTS    16

#define NO_OF_ELEMENTS(array)    (sizeof(array)/sizeof(array[0]))

int main(int argc, char *argv[])
{
    int32_t i;
    foo_t *things[NO_OF_OBJECTS];
    int32_t nspare;

    MAL_OPEN(NO_OF_OBJECTS);

    for (i = 0; i < NO_OF_OBJECTS; i++)
    {
        things[i] = MAL_MALLOC(sizeof(foo_t));
    }

    for (i = 0; i < NO_OF_OBJECTS; i++)
    {
        printf("%p %s\n", (void *)things[i],
                MAL_IS_VALID(things[i]) ? "valid" : "invalid");
    }


    printf("%p %s\n", (void *)0xDEADBEEF,
            MAL_IS_VALID((void *)0xDEADBEEF) ? "valid" : "invalid");

    for (i = 0; i < NO_OF_OBJECTS; i++)
    {
        MAL_FREE(things[i]);
    }

    for (i = 0; i < NO_OF_OBJECTS; i++)
    {
        printf("%p %s\n", (void *)things[i],
                MAL_IS_VALID(things[i]) ? "valid" : "invalid");
    }

    nspare = MAL_CLOSE();
    printf("Number of unreclaimed pointers: %d.\n", nspare);

    return 0;
}

