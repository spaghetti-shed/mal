/******************************************************************************
 * mal - memory allocation library (private header)
 * Tue Feb 12 21:59:27 GMT 2013
 *
 * Copyright (C) 2013-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of mal.
 *
 * mal is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * mal is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with mal; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2013-02-18 Accessors for pointer info array.
 * 2013-03-05 Accessors for setting internal counts.
 * 2013-03-14 Find existing pointers.
 * 2013-03-16 Internal malloc and free routines.
 * 2013-05-31 Add size field to mal_ptr_info structure.
 * 2013-06-09 Get current total size in use and max (peak) size.
 * 2013-07-04 Increase and decrease total size in use and update peak size.
 * 2022-12-28 Move get and set_debug_level() into public interface.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>

void *mal_counting_malloc(size_t size);
void mal_counting_exit(int32_t status);
void mal_counting_free(void *ptr);

int32_t mal_get_counting_malloc_count(void);
void    mal_set_counting_malloc_count(int32_t count);
int32_t mal_get_counting_free_count(void);
void    mal_set_counting_free_count(int32_t count);
int32_t mal_get_counting_exit_count(void);
void    mal_set_counting_exit_count(int32_t count);
int32_t mal_get_counting_malloc_balance(void);
int32_t mal_get_internal_malloc_balance(void);
int32_t mal_get_register_count(void);
size_t  mal_get_total_size_in_use(void);
void    mal_set_total_size_in_use(size_t size);

int32_t mal_dec_total_size_in_use(size_t size);
void    mal_inc_total_size_in_use(size_t size);

size_t  mal_get_max_total_size_used(void);
void    mal_set_max_total_size_used(size_t size);

int32_t mal_index_is_valid(int32_t index);
void   *mal_get_ptr_info_ptr(int32_t index);
void    mal_set_ptr_info_ptr(int32_t index, void *ptr);
size_t  mal_get_ptr_info_size(int32_t index);
void    mal_set_ptr_info_size(int32_t index, size_t size);
int32_t mal_get_ptr_info_allocated(int32_t index);
void    mal_set_ptr_info_allocated(int32_t index, int32_t flag);
int32_t mal_get_ptr_info_seen_count(int32_t index);
void    mal_set_ptr_info_seen_count(int32_t index, int32_t count);
void    mal_inc_ptr_info_seen_count(int32_t index);
char   *mal_get_ptr_info_alloc_str(int32_t index);
void    mal_set_ptr_info_alloc_str(int32_t index, char *str);
size_t  mal_get_ptr_info_alloc_str_len(int32_t index);
void    mal_set_ptr_info_alloc_str_len(int32_t index, size_t len);
char   *mal_get_ptr_info_free_str(int32_t index);
void    mal_set_ptr_info_free_str(int32_t index, char *str);
size_t  mal_get_ptr_info_free_str_len(int32_t index);
void    mal_set_ptr_info_free_str_len(int32_t index, size_t len);

int32_t mal_find_ptr(void *p);

