/******************************************************************************
 * mal - memory allocation library
 * Tue Feb 12 21:59:27 GMT 2013
 *
 * Copyright (C) 2013-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of mal.
 *
 * mal is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * mal is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with mal; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2013-02-18 Accessors for pointer info array.
 * 2013-03-05 Accessors for setting internal counts.
 * 2013-03-16 Internal malloc and free routines.
 * 2013-03-20 Add functions to allocate and free with text labels.
 * 2013-03-23 Add mal_list() to list all know pointers to stdout.
 * 2013-05-31 Add size field to mal_ptr_info structure.
 * 2013-06-09 Get current total size in use and max (peak) size.
 * 2013-07-04 Increase and decrease total size in use and update peak size.
 * 2013-07-20 Add mal_is_valid() to check whether a pointer is known and in
 *            use.
 * 2013-07-22 Replace most printf()s with calls to timber logging and
 *            diagnostic output routines.
 * 2014-05-28 Improvements.
 * 2015-06-13 Reduce console log verbosity.
 * 2016-11-01 Suppress nused message in mal_close() when debugging disabled.
 * 2020-01-18 Add mal_posix_memalign().
 * 2020-01-19 Add mal_multimalloc().
 * 2020-01-19 Add mal_multifree().
 * 2022-12-28 Move get and set_debug_level() into public interface.
 * 2023-01-07 Add accessor to get number of pointers.
 * 2024-09-15 Add wrapper for calloc().
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "cutl.h"
#include "timber.h"

#include "mal.h"
#include "mal_private.h"

#ifndef NO_OF_ELEMENTS
#define NO_OF_ELEMENTS(array)    (sizeof(array)/sizeof(array[0]))
#endif

typedef struct
{
    void *ptr;
    size_t size;
    int32_t allocated;
    int32_t seen_count;
    char *alloc_str;
    size_t alloc_str_len;
    char *free_str;
    size_t free_str_len;
}
mal_ptr_info;

static mal_ptr_info *ptrs;
static int32_t nptrs;
static int32_t next_ptr;

static size_t total_size_in_use;
static size_t max_total_size_used;

static int32_t internal_malloc_count;
static int32_t internal_free_count;

static int32_t malloc_count;
static int32_t free_count;
static int32_t exit_count;
static int32_t leftover_count;
static int32_t debug_level;
static int32_t register_count;

/*
 * Public accessors.
 */
int32_t mal_get_nptrs(void)
{
    return nptrs;
}

int32_t mal_get_next_ptr(void)
{
    return next_ptr;
}

/******************************************************************************/
/* Internal memory functions.                                                 */
/******************************************************************************/
static void *internal_counting_malloc(size_t size)
{
    void *ptr;

    ptr = malloc(size);
    internal_malloc_count++;

    return ptr;
}

void internal_counting_free(void *ptr)
{
    free(ptr);
    internal_free_count++;

    if (internal_free_count > internal_malloc_count)
    {
        TDR_DIAG_ERROR("Too many internal frees (mallocs: %d, frees: %d).\n", internal_malloc_count, internal_free_count);
        exit(2);
    }
}

int32_t mal_get_internal_malloc_balance(void)
{
    return internal_malloc_count - internal_free_count;
}

/******************************************************************************/
/* Local functions.                                                           */
/******************************************************************************/
void *mal_counting_malloc(size_t size)
{
    void *ptr;

    ptr = malloc(size);
    malloc_count++;

    return ptr;
}

void *mal_counting_calloc(size_t nmemb, size_t size)
{
    void *ptr;

    ptr = calloc(nmemb, size);
    malloc_count++;

    return ptr;
}

int32_t mal_counting_posix_memalign(void **ptr, size_t alignment, size_t size)
{
    int32_t retcode;

    retcode = posix_memalign(ptr, alignment, size);
    malloc_count++;

    return retcode;
}

void mal_counting_exit(int32_t status)
{
    exit_count++;
#ifndef UNIT_TEST
    exit(status);
#endif
}

void mal_counting_free(void *ptr)
{
    free(ptr);
    free_count++;

    if (free_count > (malloc_count + register_count))
    {
        TDR_DIAG_ERROR("Too many frees (mallocs: %d, registrations: %d, frees: %d).\n", malloc_count, register_count, free_count);
        mal_counting_exit(2);
    }
}

int32_t mal_get_counting_malloc_count(void)
{
    return malloc_count;
}

void mal_set_counting_malloc_count(int32_t count)
{
    malloc_count = count;
}

int32_t mal_get_counting_free_count(void)
{
    return free_count;
}

void mal_set_counting_free_count(int32_t count)
{
    free_count = count;
}

int32_t mal_get_counting_exit_count(void)
{
    return exit_count;
}

void mal_set_counting_exit_count(int32_t count)
{
    exit_count = count;
}

int32_t mal_get_counting_malloc_balance(void)
{
    return (malloc_count + register_count) - free_count;
}

void mal_set_register_count(int32_t count)
{
    register_count = count;
}

int32_t mal_get_register_count(void)
{
    return register_count;
}

size_t mal_get_total_size_in_use(void)
{
    return total_size_in_use;
}

void mal_set_total_size_in_use(size_t size)
{
    total_size_in_use = size;
}

size_t mal_get_max_total_size_used(void)
{   
    return max_total_size_used;
}

void mal_set_max_total_size_used(size_t size)
{
    max_total_size_used = size;
}


int32_t mal_index_is_valid(int32_t index)
{
    int32_t retcode = 0;

    if ((index >= 0) && (index < nptrs))
    {
        retcode = 1;
    }

    return retcode;
}

void *mal_get_ptr_info_ptr(int32_t index)
{
    return ptrs[index].ptr;
}

void mal_set_ptr_info_ptr(int32_t index, void *ptr)
{
    ptrs[index].ptr = ptr;
}

size_t mal_get_ptr_info_size(int32_t index)
{
    return ptrs[index].size;
}

void mal_set_ptr_info_size(int32_t index, size_t size)
{
    ptrs[index].size = size;
}

int32_t mal_get_ptr_info_allocated(int32_t index)
{
    return ptrs[index].allocated;
}

void mal_set_ptr_info_allocated(int32_t index, int32_t flag)
{
    ptrs[index].allocated = flag;
}

int32_t mal_get_ptr_info_seen_count(int32_t index)
{
    return ptrs[index].seen_count;
}

void mal_inc_ptr_info_seen_count(int32_t index)
{
    ptrs[index].seen_count++;
}

void mal_set_ptr_info_seen_count(int32_t index, int32_t count)
{
    ptrs[index].seen_count = count;
}

char *mal_get_ptr_info_alloc_str(int32_t index)
{
    return ptrs[index].alloc_str;
}

void mal_set_ptr_info_alloc_str(int32_t index, char *str)
{
    ptrs[index].alloc_str = str;
}

size_t mal_get_ptr_info_alloc_str_len(int32_t index)
{
    return ptrs[index].alloc_str_len;
}

void mal_set_ptr_info_alloc_str_len(int32_t index, size_t len)
{
    ptrs[index].alloc_str_len = len;
}

char *mal_get_ptr_info_free_str(int32_t index)
{
    return ptrs[index].free_str;
}

void mal_set_ptr_info_free_str(int32_t index, char *str)
{
    ptrs[index].free_str = str;
}

size_t mal_get_ptr_info_free_str_len(int32_t index)
{
    return ptrs[index].free_str_len;
}

void mal_set_ptr_info_free_str_len(int32_t index, size_t len)
{
    ptrs[index].free_str_len = len;
}

void mal_init_ptr_info(void)
{
    int32_t i;

    for (i = 0; i < nptrs; i++)
    {
        mal_set_ptr_info_ptr(i, NULL);
        mal_set_ptr_info_size(i, 0);
        mal_set_ptr_info_allocated(i, 0);
        mal_set_ptr_info_seen_count(i, 0);
        mal_set_ptr_info_alloc_str(i, NULL);
        mal_set_ptr_info_alloc_str_len(i, 0);
        mal_set_ptr_info_free_str(i, NULL);
        mal_set_ptr_info_free_str_len(i, 0);
    }
}

/******************************************************************************/
/* Public functions.                                                          */
/******************************************************************************/

int32_t mal_get_debug_level(void)
{
    return debug_level;
}

void mal_set_debug_level(int32_t level)
{
    debug_level = level;
}

int32_t mal_open(int32_t no_of_ptrs)
{
    int32_t n = 0;

    TDR_TIME_INIT();

    if (NULL != ptrs)
    {
        TDR_DIAG_ERROR("mal already open.\n");
        mal_counting_exit(2);
    }
    else
    {
        if (0 == no_of_ptrs)
        {
            n = MAL_DEFAULT_PTRS;
        }
        else
        {
            n = no_of_ptrs;
        }

        mal_set_counting_malloc_count(0);
        mal_set_counting_free_count(0);
        mal_set_register_count(0);
        mal_set_total_size_in_use(0);
        mal_set_max_total_size_used(0);

        if (NULL == (ptrs = internal_counting_malloc(n * sizeof(mal_ptr_info))))
        {
            TDR_DIAG_ERROR("internal_counting_malloc() failed.\n");
            n = 0;
        }
        else
        {
            nptrs = n;
            next_ptr = 0;
            mal_init_ptr_info();
        }
    }

    return n;
}

int32_t mal_free_leftovers(void)
{
    int32_t i;
    int32_t count = 0;
    void *ptr;
    char *malloc_str;
    char *free_str;

    for (i = 0; i < nptrs; i++)
    {
        ptr = mal_get_ptr_info_ptr(i);
        malloc_str = mal_get_ptr_info_alloc_str(i);
        free_str = mal_get_ptr_info_free_str(i);

        if (1 == mal_get_ptr_info_allocated(i))
        {
            count++;
            mal_counting_free(ptr);
            TDR_DIAG_INFO("Leftover: ");
            TDR_DIAG_TERSE("%p", ptr);
            TDR_DIAG_TERSE(" allocated: %s", malloc_str ? malloc_str : "unknown");
            TDR_DIAG_TERSE("\n");
        }
        if (debug_level > MAL_DEBUG_LEVEL_OFF)
        {
            TDR_DIAG_INFO("%p", ptr);
            TDR_DIAG_TERSE(" allocated: %s", malloc_str ? malloc_str : "unknown");
            TDR_DIAG_TERSE(" freed: %s", free_str ? free_str : "unknown");
            TDR_DIAG_TERSE("\n");
        }
        if (NULL != malloc_str)
        {
            internal_counting_free(malloc_str);
            mal_set_ptr_info_alloc_str(i, NULL);
            mal_set_ptr_info_alloc_str_len(i, 0);
        }
        if (NULL != free_str)
        {
            internal_counting_free(free_str);
            mal_set_ptr_info_free_str(i, NULL);
            mal_set_ptr_info_free_str_len(i, 0);
        }
    }

    return count;
}

int32_t mal_close(void)
{
    int nfreed = 0;
    int nused;
    int internal;

    if (NULL == ptrs)
    {
        TDR_DIAG_ERROR("failed (not open).\n");
        mal_counting_exit(2);
    }
    else if (nptrs == 0)
    {
        TDR_DIAG_ERROR("failed (no of pointers is 0).\n");
        mal_counting_exit(2);
    }
    else
    {
        nused = mal_list();

        if (debug_level > MAL_DEBUG_LEVEL_OFF)
        {
            TDR_DIAG_INFO("nused: %d.\n", nused);
        }

        leftover_count = mal_free_leftovers();
        nfreed = leftover_count;
        internal_counting_free(ptrs);
        ptrs = NULL;
        nptrs = 0;
        internal = mal_get_internal_malloc_balance();
        if (0 != internal)
        {
            TDR_DIAG_ERROR("Internal malloc balance is %d.\n", internal);
            mal_counting_exit(2);
        }
    }

    return nfreed;
}

int32_t mal_find_ptr(void *p)
{
    int32_t result = -1;
    int32_t pos = next_ptr - 1;

    while (pos >= 0)
    {
        if (ptrs[pos].ptr == p)
        {
            result = pos;
            break;
        }
        pos--;
    }

    return result;
}

int32_t mal_dec_total_size_in_use(size_t size)
{
    int32_t result = 0;
    size_t current;

    current = mal_get_total_size_in_use();
    current -= size;

    mal_set_total_size_in_use(current);

    if (current < 0)
    {
        TDR_DIAG_ERROR("Internal memory in use count negative: %d.\n", (int32_t)current);
        result = -1;
    }

    return result;
}

void mal_inc_total_size_in_use(size_t size)
{
    size_t current = mal_get_total_size_in_use() + size;

    mal_set_total_size_in_use(current);

    if (current > mal_get_max_total_size_used())
    {
        mal_set_max_total_size_used(current);
    }
}

void *mal_register(void *ptr, size_t size)
{
    void *result = NULL;
    int32_t existing;

    if (next_ptr == nptrs)
    {
        TDR_DIAG_ERROR("mal ptrs array full.\n");
        mal_counting_exit(2);
    }
    else if (NULL == ptr)
    {
        TDR_DIAG_ERROR("Attempt to register NULL pointer.\n");
        mal_counting_exit(2);
    }
    else
    {
        if (-1 != (existing = mal_find_ptr(ptr)))
        {
            if (mal_get_ptr_info_allocated(existing))
            {
                TDR_DIAG_ERROR("address %p already in use.\n", ptr);
                mal_counting_exit(2);
            }
        }
        mal_set_ptr_info_ptr(next_ptr, ptr);
        mal_set_ptr_info_size(next_ptr, size);
        mal_inc_total_size_in_use(size);
        mal_set_ptr_info_allocated(next_ptr, 1);
        mal_inc_ptr_info_seen_count(next_ptr);
        next_ptr++;
        register_count++;
        result = ptr;
    }

    return result;
}

void *mal_malloc(size_t size)
{
    void *result = NULL;
    void *new_ptr;
    int32_t existing;

    if (next_ptr == nptrs)
    {
        TDR_DIAG_ERROR("mal ptrs array full.\n");
        mal_counting_exit(2);
    }
    else if (NULL == (new_ptr = mal_counting_malloc(size)))
    {
        TDR_DIAG_ERROR("counting_malloc() failed.\n");
        mal_counting_exit(2);
    }
    else
    {
        if (-1 != (existing = mal_find_ptr(new_ptr)))
        {
            if (mal_get_ptr_info_allocated(existing))
            {
                TDR_DIAG_ERROR("address %p already in use.\n", new_ptr);
                mal_counting_exit(2);
            }
        }
        mal_set_ptr_info_ptr(next_ptr, new_ptr);
        mal_set_ptr_info_size(next_ptr, size);
        mal_inc_total_size_in_use(size);
        mal_set_ptr_info_allocated(next_ptr, 1);
        mal_inc_ptr_info_seen_count(next_ptr);
        next_ptr++;
        result = new_ptr;
    }

    return result;
}

void *mal_calloc(size_t nmemb, size_t size)
{
    void *result = NULL;
    void *new_ptr;
    int32_t existing;

    if (next_ptr == nptrs)
    {
        TDR_DIAG_ERROR("mal ptrs array full.\n");
        mal_counting_exit(2);
    }
    else if (NULL == (new_ptr = mal_counting_calloc(nmemb, size)))
    {
        TDR_DIAG_ERROR("counting_calloc() failed.\n");
        mal_counting_exit(2);
    }
    else
    {
        if (-1 != (existing = mal_find_ptr(new_ptr)))
        {
            if (mal_get_ptr_info_allocated(existing))
            {
                TDR_DIAG_ERROR("address %p already in use.\n", new_ptr);
                mal_counting_exit(2);
            }
        }
        mal_set_ptr_info_ptr(next_ptr, new_ptr);
        mal_set_ptr_info_size(next_ptr, size);
        mal_inc_total_size_in_use(size);
        mal_set_ptr_info_allocated(next_ptr, 1);
        mal_inc_ptr_info_seen_count(next_ptr);
        next_ptr++;
        result = new_ptr;
    }

    return result;
}

void mal_free(void *ptr)
{
    int32_t index;
    char *malloc_str;
    char *free_str;

    if (-1 == (index = mal_find_ptr(ptr)))
    {
        TDR_DIAG_ERROR("mal_free() unknown pointer %p.\n", ptr);
        mal_counting_exit(2);
    }
    else if (0 == mal_get_ptr_info_allocated(index))
    {
        TDR_DIAG_ERROR("mal_free() previously deallocated pointer %p", ptr);
        malloc_str = mal_get_ptr_info_alloc_str(index);
        free_str = mal_get_ptr_info_free_str(index);
        TDR_DIAG_TERSE(" allocated: %s", malloc_str ? malloc_str : "unknown");
        TDR_DIAG_TERSE(" freed: %s", free_str ? free_str : "unknown");
        TDR_DIAG_TERSE("\n");
        mal_counting_exit(2);
    }
    else
    {
        mal_counting_free(ptr);
        mal_set_ptr_info_allocated(index, 0);
        if (-1 == mal_dec_total_size_in_use(mal_get_ptr_info_size(index)))
        {
            TDR_DIAG_ERROR("size_in_use underflow.\n");
        }
    }
}

#define MAX_INT32_DIGITS    10

void *mal_malloc_with_labels(size_t size, const char *file, const char *function, int32_t line)
{
    void *ptr = NULL;
    int32_t ptr_index;
    char *label;
    char line_str[MAX_INT32_DIGITS + 1];
    int32_t length;

    if (NULL == file)
    {
    }
    else if (NULL == function)
    {
    }
    else
    {
        length = strlen(file) + 1 + strlen(function) + 1 + MAX_INT32_DIGITS + 1;
        label = internal_counting_malloc((size_t)length);
        if (NULL == label)
        {
        }
        else
        {
            strcpy(label, file);
            strcat(label, ":");
            strcat(label, function);
            strcat(label, ":");
            snprintf(line_str, (size_t)NO_OF_ELEMENTS(line_str), "%u", (unsigned int)line);
            strcat(label, line_str);

            ptr = mal_malloc(size);
            /* FIXME: Change behaviour for returning a NULL pointer. */
            if (NULL == ptr)
            {
                TDR_DIAG_TERSE("%s:ERROR: mal_malloc_with_labels() failed.\n", label);
                internal_counting_free(label); /* This isn't completely daft. */
                mal_counting_exit(2);
            }
            else
            {
                ptr_index = mal_find_ptr(ptr);
                mal_set_ptr_info_alloc_str(ptr_index, label);
            }
        }
    }
    
    return ptr;
}

void *mal_calloc_with_labels(size_t nmemb, size_t size, const char *file, const char *function, int32_t line)
{
    void *ptr = NULL;
    int32_t ptr_index;
    char *label;
    char line_str[MAX_INT32_DIGITS + 1];
    int32_t length;

    if (NULL == file)
    {
    }
    else if (NULL == function)
    {
    }
    else
    {
        length = strlen(file) + 1 + strlen(function) + 1 + MAX_INT32_DIGITS + 1;
        label = internal_counting_malloc((size_t)length);
        if (NULL == label)
        {
        }
        else
        {
            strcpy(label, file);
            strcat(label, ":");
            strcat(label, function);
            strcat(label, ":");
            snprintf(line_str, (size_t)NO_OF_ELEMENTS(line_str), "%u", (unsigned int)line);
            strcat(label, line_str);

            ptr = mal_calloc(nmemb, size);
            /* FIXME: Change behaviour for returning a NULL pointer. */
            if (NULL == ptr)
            {
                TDR_DIAG_TERSE("%s:ERROR: mal_malloc_with_labels() failed.\n", label);
                internal_counting_free(label); /* This isn't completely daft. */
                mal_counting_exit(2);
            }
            else
            {
                ptr_index = mal_find_ptr(ptr);
                mal_set_ptr_info_alloc_str(ptr_index, label);
            }
        }
    }

    return ptr;
}

void mal_free_with_labels(void *ptr, const char *file, const char *function, int32_t line)
{
    int32_t ptr_index;
    char *label;
    char line_str[MAX_INT32_DIGITS + 1];
    int32_t length;

    if (NULL == file)
    {
    }
    else if (NULL == function)
    {
    }
    else
    {
        length = strlen(file) + 1 + strlen(function) + 1 + MAX_INT32_DIGITS + 1;
        label = internal_counting_malloc((size_t)length);
        if (NULL == label)
        {
        }
        else
        {
            strcpy(label, file);
            strcat(label, ":");
            strcat(label, function);
            strcat(label, ":");
            snprintf(line_str, (size_t)NO_OF_ELEMENTS(line_str), "%u", (unsigned int)line);
            strcat(label, line_str);

            ptr_index = mal_find_ptr(ptr);
            if (-1 == ptr_index)
            {
                TDR_DIAG_TERSE("%s:ERROR: mal_free_with_labels() failed.\n", label);
                internal_counting_free(label); /* This isn't completely daft. */
                mal_counting_exit(2);
            }
            else
            {
                mal_set_ptr_info_free_str(ptr_index, label);
                mal_free(ptr);
            }
        }
    }
}

void *mal_register_with_labels(void *ptr, size_t size, const char *file, const char *function, int32_t line)
{
    int32_t ptr_index;
    char *label;
    char line_str[MAX_INT32_DIGITS + 1];
    int32_t length;
    void *output = NULL;

    if (NULL == file)
    {
    }
    else if (NULL == function)
    {
    }
    else
    {
        length = strlen(file) + 1 + strlen(function) + 1 + MAX_INT32_DIGITS + 1;
        label = internal_counting_malloc((size_t)length);
        if (NULL == label)
        {
        }
        else
        {
            strcpy(label, file);
            strcat(label, ":");
            strcat(label, function);
            strcat(label, ":");
            snprintf(line_str, (size_t)NO_OF_ELEMENTS(line_str), "%u", (unsigned int)line);
            strcat(label, line_str);

            output = mal_register(ptr, size);
            /* FIXME: Change behaviour for returning a NULL pointer. */
            if (NULL == output)
            {
                TDR_DIAG_TERSE("%s:ERROR: NULL pointer.\n", label);
                internal_counting_free(label); /* This isn't completely daft. */
                mal_counting_exit(2);
            }
            else
            {
                ptr_index = mal_find_ptr(output);
                mal_set_ptr_info_alloc_str(ptr_index, label);
            }
        }
    }

    return output;
}

/*
 * FIXME: Rename this function!
 */
int32_t mal_list(void)
{
    int32_t i;
    int32_t count = 0;
    void *ptr;
    char *malloc_str;
    char *free_str;

    for (i = 0; i < nptrs; i++)
    {
        if (NULL != (ptr = mal_get_ptr_info_ptr(i)))
        {
            count++;
            if (debug_level > MAL_DEBUG_LEVEL_OFF)
            {
                malloc_str = mal_get_ptr_info_alloc_str(i);
                TDR_DIAG_TERSE("%p", ptr);
                TDR_DIAG_TERSE(" allocated: %s", malloc_str ? malloc_str : "unknown");

                if (0 == mal_get_ptr_info_allocated(i))
                {
                    free_str = mal_get_ptr_info_free_str(i);
                    TDR_DIAG_TERSE(" freed: %s", free_str ? free_str : "unknown");
                }

                TDR_DIAG_TERSE("\n");
            }
        }
    }

    return count;
}

int32_t mal_is_valid(void *ptr)
{
    int retcode = 0;
    int32_t existing;

    if (-1 != (existing = mal_find_ptr(ptr)))
    {
        if (mal_get_ptr_info_allocated(existing))
        {
            retcode = 1;
        }
    }

    return retcode;
}

int32_t mal_posix_memalign(void **ptr, size_t alignment, size_t size)
{
    int32_t retcode = 0;
    void *new_ptr;
    int32_t existing;

    if (next_ptr == nptrs)
    {
        TDR_DIAG_ERROR("mal ptrs array full.\n");
        mal_counting_exit(2);
    }
    else if (0 != mal_counting_posix_memalign(&new_ptr, alignment, size))
    {
        TDR_DIAG_ERROR("counting_posix_memalign() failed.\n");
        mal_counting_exit(2);
    }
    else
    {
        if (-1 != (existing = mal_find_ptr(new_ptr)))
        {
            if (mal_get_ptr_info_allocated(existing))
            {
                TDR_DIAG_ERROR("address %p already in use.\n", new_ptr);
                mal_counting_exit(2);
            }
        }
        mal_set_ptr_info_ptr(next_ptr, new_ptr);
        mal_set_ptr_info_size(next_ptr, size);
        mal_inc_total_size_in_use(size);
        mal_set_ptr_info_allocated(next_ptr, 1);
        mal_inc_ptr_info_seen_count(next_ptr);
        next_ptr++;
        *ptr = new_ptr;
    }

    return retcode;
}

/*
 * mal_multimalloc(void **ptr, size_t size, int32_t number)
 *
 * Allocate memory for an array of number objects of size size and an array of
 * pointers to each object in the array. Populate the array of pointers and
 * assign its address to ptr.
 *
 * No alignment of objects is performed.
 * The internal pointers are not added to the table of valid pointers.
 * To free the allocated storage, use the pointer to the array of pointers.
 */
int32_t mal_multimalloc(void ***ptr, size_t size, int32_t number)
{
    int32_t retcode = -1;
    void **pointers;
    void *data;
    int32_t i;

    if (0 == number)
    {
    }
    else if (0 == size)
    {
    }
    else if (NULL == (pointers = mal_malloc(number * sizeof(void *))))
    {
        TDR_DIAG_ERROR("Failed to allocate array of pointers.\n");
        mal_counting_exit(2);
    }
    else if (NULL == (data = mal_malloc(number * size)))
    {
        TDR_DIAG_ERROR("Failed to allocate data area.\n");
        mal_free(pointers);
        mal_counting_exit(2);
    }
    else
    {
        for (i = 0; i < number; i++)
        {
            pointers[i] = data + (i * size);
        }
        *ptr = pointers;
        retcode = 0;
    }

    return retcode;
}

void mal_multifree(void **ptr)
{
    mal_free(ptr[0]);
    mal_free(ptr);
}

