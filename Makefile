#
# Makefile for mal - memory allocation library.
# Tue Feb 12 21:59:27 GMT 2013 
#
# Copyright (C) 2013-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
# This file is part of mal.
#
# mal is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# mal is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with mal; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
#
# 2013-03-23 Add mal_demo.c.
# 2013-07-09 Update to use unit test assersions and functions from cutl.
# 2013-07-21 Add mal_demo_valid.c.
# 2013-07-22 Remove -pedantic compiler switch for timber.h (logging and
#            diagnostic output routines).
# 2014-05-28 Use dynamic versions of libtimber and libcutl in /usr/local/lib64.
# 2014-08-17 Install target.
# 2022-06-14 Add uninstall target.
# 2022-12-28 Add pass-through macros and test target (minimal).
# 2022-12-29 Add demo for MAL_REGISTER.
# 2023-05-20 Exercise the C++ interface (cpp target).
#

CC=gcc
CFLAGS=-Wall -Werror -O -fPIC
SHAREDFLAGS=-shared
CPPTESTFLAGS=-DTESTING -DUNIT_TEST
TOPDIR=$(shell pwd)/..
INCPATH = /usr/local/include
LIBPATH = /usr/local/lib64
LIBS = -lcutl -ltimber
INSTALL_DIR=/usr/local
INSTALL_INC=$(INSTALL_DIR)/include
INSTALL_LIB=$(INSTALL_DIR)/lib64

CXX=g++
CXXFLAGS=-Wall -Werror -O

all: test main demo passthrough shared

test: mal_test
	./mal_test

demo: mal_demo mal_demo_leak mal_demo_double_free mal_demo_valid mal_demo_register

main: mal

mal: mal.h mal.c
	$(CC) $(CFLAGS) -c -I$(INCPATH) mal.c
#	$(CC) $(CFLAGS) -o mal mal.o $(LIBS)

mal_test: mal_test.c mal.c mal.h
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -I$(INCPATH) -o mal_testing.o -c mal.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -I$(INCPATH) -c mal_test.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o mal_test mal_test.o mal_testing.o $(LIBS)

shared: mal test
	$(CC) $(CFLAGS) $(SHAREDFLAGS) -o libmal.so mal.o

mal_demo: mal mal_demo.c
	$(CC) $(CFLAGS) -c mal_demo.c
	$(CC) $(CFLAGS) -o mal_demo mal_demo.o mal.o $(LIBS)

passthrough: mal mal_demo.c
	$(CC) $(CFLAGS) -DMAL_PASSTHROUGH -c mal_demo.c
	$(CC) $(CFLAGS) -o mal_demo_passthrough mal_demo.o mal.o $(LIBS)

mal_demo_leak: mal mal_demo_leak.c
	$(CC) $(CFLAGS) -c mal_demo_leak.c
	$(CC) $(CFLAGS) -o mal_demo_leak mal_demo_leak.o mal.o $(LIBS)

mal_demo_double_free: mal mal_demo_double_free.c
	$(CC) $(CFLAGS) -c mal_demo_double_free.c
	$(CC) $(CFLAGS) -o mal_demo_double_free mal_demo_double_free.o mal.o $(LIBS)

mal_demo_valid: mal mal_demo_valid.c
	$(CC) $(CFLAGS) -c mal_demo_valid.c
	$(CC) $(CFLAGS) -o mal_demo_valid mal_demo_valid.o mal.o $(LIBS)

mal_demo_register: mal mal_demo_register.c
	$(CC) $(CFLAGS) -c mal_demo_register.c
	$(CC) $(CFLAGS) -o mal_demo_register mal_demo_register.o mal.o $(LIBS)

cpp: mal mal_demo_cpp.cpp
	$(CXX) $(CXXFLAGS) -c mal_demo_cpp.cpp
	$(CXX) $(CXXFLAGS) -o mal_demo_cpp mal_demo_cpp.o mal.o $(LIBS)

install: shared
	mkdir -p $(INSTALL_INC)
	cp mal.h $(INSTALL_INC)
	mkdir -p $(INSTALL_LIB)
	cp libmal.so $(INSTALL_LIB)

uninstall:
	rm -f $(INSTALL_INC)/mal.h
	rm -f $(INSTALL_LIB)/libmal.so

clean:
	rm -f *.o
	rm -f mal_demo
	rm -f mal_demo_cpp
	rm -f mal_demo_leak
	rm -f mal_demo_double_free
	rm -f mal_demo_valid
	rm -f mal_demo_register
	rm -f mal_demo_passthrough
	rm -f mal_test
	rm -f mal
	rm -f libmal.so

