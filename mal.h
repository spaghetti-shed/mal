/******************************************************************************
 * mal - memory allocation library (header)
 * Tue Feb 12 21:59:27 GMT 2013
 *
 * Copyright (C) 2013-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of mal.
 *
 * mal is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * mal is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with mal; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2013-03-20 Add functions to allocate and free with text labels.
 * 2013-03-23 Add mal_list() to list all know pointers to stdout.
 * 2013-03-20 Add mal_is_valid() to check whether a pointer is known and in
 *            use.
 * 2020-01-18 Add mal_posix_memalign().
 * 2020-01-19 Add mal_multimalloc().
 * 2020-01-20 Add mal_multifree().
 * 2022-12-28 Pass-through macros.
 *            Move get and set_debug_level() into public interface.
 * 2022-12-29 Add mal_register_with_labels().
 *            Add MAL_REGISTER to public API.
 * 2023-01-07 Add accessor to get number of pointers.
 * 2024-09-15 Add wrapper for calloc().
 ******************************************************************************/

#ifndef __MAL_H__
#define __MAL_H__

#define MAL_DEFAULT_PTRS            1024

#define MAL_DEBUG_LEVEL_OFF         0
#define MAL_DEBUG_LEVEL_MALLOC      1
#define MAL_DEBUG_LEVEL_FREE        2
#define MAL_DEBUG_LEVEL_ALL         3

#ifndef MAL_PASSTHROUGH

#define MAL_OPEN(n) \
mal_open(n)

#define MAL_CLOSE() \
mal_close()

#define MAL_MALLOC(size) \
mal_malloc_with_labels(size, __FILE__, __FUNCTION__, __LINE__)

#define MAL_CALLOC(nmemb, size) \
mal_calloc_with_labels(nmemb, size, __FILE__, __FUNCTION__, __LINE__)

#define MAL_REGISTER(ptr, size) \
mal_register_with_labels(ptr, size, __FILE__, __FUNCTION__, __LINE__)

#define MAL_FREE(ptr) \
mal_free_with_labels(ptr, __FILE__, __FUNCTION__, __LINE__)

#define MAL_LIST() \
mal_list()

#define MAL_IS_VALID(ptr) \
mal_is_valid(ptr)

#define MAL_SET_DEBUG_LEVEL(level) \
mal_set_debug_level(level)

#define MAL_GET_DEBUG_LEVEL() \
mal_get_debug_level()

#define MAL_GET_NPTRS() \
mal_get_nptrs()

#define MAL_GET_NEXT_PTR() \
mal_get_next_ptr()

#else /* Pass-through interface (no overhead) */

#include <stdlib.h>

#define MAL_OPEN(n)

#define MAL_CLOSE() \
(0)

#define MAL_MALLOC(size) \
malloc(size)

#define MAL_CALLOC(nmemb, size) \
calloc(nmemb, size)

#define MAL_REGISTER(ptr, size) \
(ptr)

#define MAL_FREE(ptr) \
free(ptr)

#define MAL_LIST()

#define MAL_IS_VALID(ptr) \
(1)

#define MAL_SET_DEBUG_LEVEL(level)


#define MAL_GET_DEBUG_LEVEL() \
(0)

#define MAL_GET_NPTRS() \
(0)

#define MAL_GET_NEXT_PTR() \
(0)

#endif

#ifdef __cplusplus
extern "C"
{
#endif

int32_t mal_get_debug_level(void);
void    mal_set_debug_level(int32_t level);

int32_t mal_open(int32_t no_of_ptrs);
int32_t mal_close(void);

void *mal_register(void *ptr, size_t size);
void *mal_malloc(size_t size);
void mal_free(void *ptr);

void *mal_malloc_with_labels(size_t size, const char *file, const char *function, int32_t line);
void *mal_calloc_with_labels(size_t nmemb, size_t size, const char *file, const char *function, int32_t line);
void mal_free_with_labels(void *ptr, const char *file, const char *function, int32_t line);
void *mal_register_with_labels(void *ptr, size_t size, const char *file, const char *function, int32_t line);

int32_t mal_list(void);

int32_t mal_is_valid(void *ptr);

int32_t mal_posix_memalign(void **ptr, size_t alignment, size_t size);
int32_t mal_multimalloc(void ***ptr, size_t size, int32_t number);
void mal_multifree(void **ptr);

int32_t mal_get_nptrs(void);
int32_t mal_get_next_ptr(void);

#ifdef __cplusplus
}
#endif

#endif /* __MAL_H__ */

